# MMX - Multithreaded matrix operations on N-D matrices

Cloned version of Yuval's MMX (https://fr.mathworks.com/matlabcentral/fileexchange/37515-mmx-multithreaded-matrix-operations-on-n-d-matrices)
With a bonus implementation of MMX for complex matrix/matrix multiplication

## MMX: original implementation

MMX treats an N-D matrix of double precision values as a set of pages of 2D matrices, and performs various matrix operations on those pages. 
MMX uses multithreading over the higher dimensions to achieve good performance. Full singleton expansion is available for most operations.

`C = MMX('mult', A, B)` is equivalent to the matlab loop 
	
```matlab
	for i=1:N, 
		C(:,:,i) = A(:,:,i) * B(:,:,i); 
	end
```

### Matrix transpose

The fourth argument of MMX in multiplication mode can be used to transform either A or B or both. `C = MMX('mult', A, B, mod)` where mod is a modifier string, will transpose one or both of A and B. Possible values for mod are 'tn', 'nt' and 'tt' where 't' stands for 'transposed' and 'n' for 'not-transposed'.

### Singleton expansion

Singleton expansion is enabled on all dimensions so for example if 

```matlab
	A = randn(5,4,3,10,1); 
	B = randn(4,6,3,1 ,6); 
	C = zeros(5,6,3,10,6); 
```

then `C = mmx('mult',A,B)` equivalent to 

```matlab
	for i = 1:3 
		for j = 1:10 
			for k = 1:6 
				C(:,:,i,j,k) = A(:,:,i,j,1) * B(:,:,i,1,k); 
			end 
		end 
	end
```

### Other operators

Besides computing matrix/matrix multiplication, mmx can also compute the following operators

* `C = MMX('square', A, [])` will perform C = A*A' 
* `C = MMX('square', A, B)` will perform C = 0.5*(A*B'+B*A') 
* `C = MMX('chol', A, [])` will perform C = chol(A)
* `C = MMX('backslash', A, B)` will perform C = A\B 
* `C = MMX('backslash', A, B, 'U')` will perform C = A\B assuming that A is upper triangular
* `C = MMX('backslash', A, B, 'L')` will perform C = A\B assuming that A is lower triangular
* `C = MMX('backslash', A, B, 'P')` will perform C = A\B assuming that A is symmetric-positive-definite.

Unlike other commands, 'backslash' does not support singleton expansion. If A is square, mmx will use LU factorization, otherwise it will use QR factorization. In the underdetermined case, (i.e. when size(A,1) < size(A,2)), mmx will give the least-norm solution which is equivalent to C = pinv(A)*B, unlike matlab's mldivide.

### thread control

MMX(n) does thread control: mmx will automatically start a number of threads equal to the number of available processors, however the number can be set manually to n using the command mmx(n). mmx(0) will clear the threads from memory.

## MMXC: MMX for complex matrix/matrix multiplication

The original mmx implementation only deals with real matrices. We implemented a complex version of the mmx which can perform matrix/matrix multiplication of complex matrices.
For the complex matrix multiplication, the matrix product is computed with simple for-loops. We did not manage to use the BLAs, but for matrices up to size 10x10, the for-loops should outperform the BLAs due to having less overhead.

```matlab
	A = rand(2,2,1000) + 1i*rand(2,2,1000);
	B = rand(2,2,1000) + 1i*rand(2,2,1000);
	C = mmxc(A,B);
```

Two mmxc functions are provided: mmxc and mmxc_nothreads. mmxc uses multithreading to speed up the calculation. For small tasks below a few thousand multiply-add operations, the overhead of creating the threads and distributing the work will cause too much overhead. Therefore, we also implemented mmxc_nothreads, which runs everything on a single core without threading.

## Installation

To fully install and compile mmx, perform the following steps:

1. Browse to the folder where you want the mmx code to be saved
2. Clone the code to your computer using ```git clone git@gitlab.inria.fr:acooman/mmx.git```	
3. Open Matlab
4. Add the mmx and folder and its sub-folders to the matlab path
5. Compile mmx by running the build_mmx() function
6. Compile mmxc by running the build_mmxc() function

## Requirements

You will need a configured mex compiler. Run `mex -setup` in matlab to test this