extern "C"{
	
// ----------------------------- LAPACK --------------------------------

//  DPOTRF computes the Cholesky factorization of a real symmetric
// positive definite matrix A.
//
// The factorization has the form
//    A = U**T * U,  if UPLO = 'U', or
//    A = L  * L**T,  if UPLO = 'L',
// where U is an upper triangular matrix and L is lower triangular.
//
// This is the block version of the algorithm, calling Level 3 BLAS.
void dpotrf(
	  const char *UPLO, 
	  const ptrdiff_t *M, 
	  double A[], 
	  const ptrdiff_t *LDA,
      ptrdiff_t *INFO
	  );
	  
// DPOSV computes the solution to a real system of linear equations
//    A * X = B,
// where A is an N-by-N symmetric positive definite matrix and X and B
// are N-by-NRHS matrices.
//
// The Cholesky decomposition is used to factor A as
//    A = U**T* U,  if UPLO = 'U', or
//    A = L * L**T,  if UPLO = 'L',
// where U is an upper triangular matrix and L is a lower triangular
// matrix.  The factored form of A is then used to solve the system of
// equations A * X = B.
void dposv(
	  const char *UPLO, 
	  const ptrdiff_t *N,  
	  const ptrdiff_t *NRHS, 
	  double A[], 
	  const ptrdiff_t *LDA,
      double B[], 
	  const ptrdiff_t *LDB, 
	  const ptrdiff_t *INFO
	  );

//DGESV computes the solution to a real system of linear equations
//    A * X = B,
// where A is an N-by-N matrix and X and B are N-by-NRHS matrices.
//
// The LU decomposition with partial pivoting and row interchanges is
// used to factor A as
//    A = P * L * U,
// where P is a permutation matrix, L is unit lower triangular, and U is
// upper triangular.  The factored form of A is then used to solve the
// system of equations A * X = B.	  
void dgesv(
	  const ptrdiff_t *N,  
	  const ptrdiff_t *NRHS, 
	  double A[], 
	  const ptrdiff_t *LDA,
      const ptrdiff_t *IPIV, 
	  double B[], 
	  const ptrdiff_t *LDB, 
	  const ptrdiff_t *INFO
	  ); 
	  
//DGELSY computes the minimum-norm solution to a real linear least
// squares problem:
//     minimize || A * X - B ||
// using a complete orthogonal factorization of A.  A is an M-by-N
// matrix which may be rank-deficient.
//
// Several right hand side vectors b and solution vectors x can be
// handled in a single call; they are stored as the columns of the
// M-by-NRHS right hand side matrix B and the N-by-NRHS solution
// matrix X.
//
// The routine first computes a QR factorization with column pivoting:
//     A * P = Q * [ R11 R12 ]
//                 [  0  R22 ]
// with R11 defined as the largest leading submatrix whose estimated
// condition number is less than 1/RCOND.  The order of R11, RANK,
// is the effective rank of A.
//
// Then, R22 is considered to be negligible, and R12 is annihilated
// by orthogonal transformations from the right, arriving at the
// complete orthogonal factorization:
//    A * P = Q * [ T11 0 ] * Z
//                [  0  0 ]
// The minimum-norm solution is then
//    X = P * Z**T [ inv(T11)*Q1**T*B ]
//                 [        0         ]
// where Q1 consists of the first RANK columns of Q.
//
// This routine is basically identical to the original xGELSX except
// three differences:
//   o The call to the subroutine xGEQPF has been substituted by the
//     the call to the subroutine xGEQP3. This subroutine is a Blas-3
//     version of the QR factorization with column pivoting.
//   o Matrix B (the right hand side) is updated with Blas-3.
//   o The permutation of matrix B (the right hand side) is faster and
//     more simple.
void dgelsy(
	  const ptrdiff_t *M, 
	  const ptrdiff_t *N,  
	  const ptrdiff_t *NRHS, 
	  double A[], 
	  const ptrdiff_t *LDA,
      double B[], 
	  const ptrdiff_t *LDB, 
	  const ptrdiff_t *JPVT, 
	  const double *rcond, 
	  const ptrdiff_t *rank, 
	  double work[],
      const ptrdiff_t *lwork, 
	  const ptrdiff_t *INFO
	  );


// ----------------------------- BLAS --------------------------------
	  
//DGEMM  performs one of the matrix-matrix operations
//
//    C := alpha*op( A )*op( B ) + beta*C,
//
// where  op( X ) is one of
//
//    op( X ) = X   or   op( X ) = X**T,
//
// alpha and beta are scalars, and A, B and C are matrices, with op( A )
// an m by k matrix,  op( B )  a  k by n matrix and  C an m by n matrix.
extern void dgemm(
      char   *transa,
      char   *transb,
      ptrdiff_t *m,
      ptrdiff_t *n,
      ptrdiff_t *k,
      double *alpha,
      double *a,
      ptrdiff_t *lda,
      double *b,
      ptrdiff_t *ldb,
      double *beta,
      double *c,
      ptrdiff_t *ldc
      );
	  
// DSYMM  performs one of the matrix-matrix operations
//
//    C := alpha*A*B + beta*C,
//
// or
//
//    C := alpha*B*A + beta*C,
//
// where alpha and beta are scalars,  A is a symmetric matrix and  B and
// C are  m by n matrices.
extern void dsymm(
      char   *side,
      char   *uplo,
      ptrdiff_t *m,
      ptrdiff_t *n,
      double *alpha,
      double *a,
      ptrdiff_t *lda,
      double *b,
      ptrdiff_t *ldb,
      double *beta,
      double *c,
      ptrdiff_t *ldc
      );
	  
//DSYRK  performs one of the symmetric rank k operations
//
//    C := alpha*A*A**T + beta*C,
//
// or
//
//    C := alpha*A**T*A + beta*C,
//
// where  alpha and beta  are scalars, C is an  n by n  symmetric matrix
// and  A  is an  n by k  matrix in the first case and a  k by n  matrix
// in the second case.
extern void dsyrk(
      char   *uplo,
      char   *trans,
      ptrdiff_t *n,
      ptrdiff_t *k,
      double *alpha,
      double *a,
      ptrdiff_t *lda,
      double *beta,
      double *c,
      ptrdiff_t *ldc
      );
	  
//DSYR2K  performs one of the symmetric rank 2k operations
//
//    C := alpha*A*B**T + alpha*B*A**T + beta*C,
//
// or
//
//    C := alpha*A**T*B + alpha*B**T*A + beta*C,
//
// where  alpha and beta  are scalars, C is an  n by n  symmetric matrix
// and  A and B  are  n by k  matrices  in the  first  case  and  k by n
// matrices in the second case.
extern void dsyr2k(
      char   *uplo,
      char   *trans,
      ptrdiff_t *n,
      ptrdiff_t *k,
      double *alpha,
      double *a,
      ptrdiff_t *lda,
      double *b,
      ptrdiff_t *ldb,
      double *beta,
      double *c,
      ptrdiff_t *ldc
      );

// DTRSM  solves one of the matrix equations
//
//    op( A )*X = alpha*B,   or   X*op( A ) = alpha*B,
//
// where alpha is a scalar, X and B are m by n matrices, A is a unit, or
// non-unit,  upper or lower triangular matrix  and  op( A )  is one  of
//
//    op( A ) = A   or   op( A ) = A**T.
//
// The matrix X is overwritten on B.
extern void dtrsm(
      char   *side,
      char   *uplo,
      char   *transa,
      char   *diag,
      ptrdiff_t *m,
      ptrdiff_t *n,
      double *alpha,
      double *a,
      ptrdiff_t *lda,
      double *b,
      ptrdiff_t *ldb
      );

}
